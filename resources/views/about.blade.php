@extends('layouts.app')

@section('content')

<div class="bg-brc">
    <div class="container">
        <div class="text-center text-white py-ara">
            <h1 class="text-head"><strong>ABOUT</strong> US</h1>
        </div>
    </div>
</div>

<div class="bg-full">
    <div class="container">
        <div class="col-12 mx-auto text-justify pb-ara">
            <h1 class="pt-ara pb-4 text-ara-red text-center">
                <strong>ARA</strong> & ASSOCIATES
            </h1>
            {{-- <hr class="bg-ara-red"> --}}
            <p>ARA & Associates Law Offices (hereinafter shall be referred to as the “Firm”) was formed in early 2013, by its founding
                members Mr. Arif and Mr. Rama. Focused on litigation and corporate matters, the Firm has developed for serving best
                interest client, work to achieve client goal under prevailing Indonesia laws and regulation, and committed to providing
                professional legal service and timely and cost-sensitive representation to our clients. We are full of young, dynamic,
                and intelligence people, operating at a definite quality management system, rendering preeminent and the world-class
                legal. services under a specified quality code, core values.</p>
            <p>We are delighted to work exceptional service and provide a high-quality service also an ability to give outstanding
                local and international advice as well as an integrated full service. In seeking a legal solution, the Firm always
                adopts progressive and proactive in the practice of the law, based on relevant local knowledge. We seek to remain
                to be Firm of choice for those who desire highly competent and responsive lawyers to assist them in many activities
                in Indonesia. We take pride in being able to deliver top-notch specialized services as mentioned above. Indeed, we
                honor the trust of our clients in appointing us. Thus we always commit and strive to give the utmost level of expertise,
                attention, and supervision to the work which we accept.</p>
            <p>The Firmare the finest professional in their field. Experienced and capable of competing in what we started. The interest
                and enthusiasm for clearing and finishing cases are our main concern. </p>
        </div>
    </div>
</div>

<div class="bg-full bg-ara-red">
    <div class="container py-ara text-white">
        <div class="row">
            <div class="col-md-5 my-3">
                <img class="img-fluid" src="{{ asset('img/at-01.jpg') }}" alt="ARA">
            </div>
            <div class="col-md-7 my-3 text-justify">
                <p><strong>Mr. Arif Fadillah Arifin, S.H., M.H. <br>Managing Partner</strong></p>
                <hr align="left" class="hr-about">
                <p>Mr. Arif graduated from the Faculty of Law Trisakti University in Jakarta, majoring Business Law in 2008, and continued his study for Masters Degree in Faculty of Law Indonesia University in Jakarta, majoring Business Law in 2012. He started his career since 2008 as a Litigation and Corporate Lawyers in Gani Djemat & Partners Law Firm. During his tenure in Gani Djemat & Partners Law Firm, he has represented many client and handle many case in the various matters based in his expertise in litigation and corporate matters.</p>
                <p>He combined legal aspect and business matters to settled complex legal cases, he believe in made strong and flexible legal opinion, legal perspective shall be considered with business aspect and business decission shall be supported with legal aspect.</p>
                <p>He also member of Indonesian Advocates Association (PERADI).</p>
            </div>
            <div class="col-md-5 my-3">
                <img src="{{ asset('img/at-02.jpg') }}" alt="ARA" class="img-fluid">
            </div>
            <div class="col-md-7 my-3 text-justify">
                <p><strong>Mr. Rama Difa, S.H., M.H. <br>Partner</strong></p>
                <hr align="left" class="hr-about">
                <p>Mr. Rama graduated from the Faculty of Law Trisakti University in Jakarta, majoring Law Practioner in 2008, and continued his study for Masters Degree in Faculty of Law Padjajaran University in Jakarta, majoring Business Law in 2012. He started his career since 2008 as a Lawyers in Farhat Abbas Law Office, then he worked as Legal Counsel in the national company which serves engineering, procurement, construction and installation in oil and gas activity. He has extensive experience in Civil Law dan Criminal Law, his ability in negotiation has brought him to become a successful negotiator in many dispute settlement on his duty as Legal Counsel. Now he becomes a very competent lawyer in litigation.</p>
                <p>He also member of Indonesian Advocates Association (PERADI).</p>
            </div>
            <hr>
            <div class="col-md-6 my-3 text-justify">
                <p><strong>Mr. Rangga Setyo Pradono, S.H. <br>Partner</strong></p>
                <hr align="left" class="hr-about">
                <p>Mr. Rangga graduated from the Faculty of Law Trisakti University in Jakarta, majoring practition in 2009, He worked as Legal Supervisor in national company that provide garment and distribution of apparel, He started his career since 2011 at DSA & Partners Law Firm. During his tenure in DSA & Partners Law Firm, he has represented many client and handle many case in the various matters based in his expertise in litigation,  Marital & Divorce Law, legal aspect and business matters to settled complex legal cases,</p>
                <p>He also Member of Indonesian Advocates Association (PERADI).</p>
            </div>
            <div class="col-md-6 my-3 text-justify">
                <p><strong>Mr. Barito Dwi Martono, S.H., M.Kn. <br>Partner / Public Notary</strong></p>
                <hr align="left" class="hr-about">
                <p>Mr. Barito graduated from the Land Administration in Padjajaran University in Bandung in 2007, and continued his study for Law Degree in Faculty of Law Borobudur University in Jakarta, majoring Practioner Law in 2009. Mr. Barito continued his study for Master Degree in Borobudur University, majoring Notaries in 2012. He started his career since 2014 as a Public Notaries, he has represented many client and handle many case in the various matters based in his expertise in land, property and corporate matters.</p>
                <p>He also member of Indonesian Public Notary Bond (Ikatan Notaris Indonesia).</p>
            </div>
            <div class="col-md-6 my-3 text-justify">
                <p><strong>Mr. Chandra Yudha Kusuma, S.H. <br>Associate</strong></p>
                <hr class="hr-about" align="left">
                <p>Mr. Chandra Yudha graduated from the Faculty of Law Diponegoro University in Semarang, 2013. He started his career since 2013 as a Litigation and Corporate Lawyers in Fajar AS, SH, MH & Partners Law Firm, and DSA & Partners since 2014. During his tenure in Fajar AS, SH, MH & Partners Law Firm, and DSA & Partners Law Firm, he has represented many client and handle many case in the various matters based in his expertise in litigation and corporate matters.</p>
                <p>He also member of Indonesian Advocates Association (PERADI).</p>
            </div>
        </div>
    </div>
</div>

@endsection
