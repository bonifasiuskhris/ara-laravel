@extends('layouts.app')

@section('content')

<div class="bg-full">
    <div class="container">
        <div class="col-md-12 pt-ara">
            <h1><strong>OUR</strong><br>ADDRESS</h1><br>
            <address>
                <p>Gedung PKMI Pusat Lantai 1, Unit 2 <br>
                    Jl.Kramat Sentiong, No. 49A, Senen, Jakarta Pusat<br>
                    <a href="mailto:mail@ara-lawoffice.com">Email: mail@ara-lawoffice.com</a>
                </p>
                <br>
            </address>
        </div>
        <div class="col-md-12">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1983.290152171002!2d106.84861260712843!3d-6.1868787995291905!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f44596e6256f%3A0x73d0952ca7f6cf36!2sJl.+Kramat+Sentiong+No.49a%2C+RT.11%2FRW.6%2C+Kramat%2C+Senen%2C+Kota+Jakarta+Pusat%2C+Daerah+Khusus+Ibukota+Jakarta+10450!5e0!3m2!1sen!2sid!4v1533833834110" width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
    </div>
</div>

@endsection
