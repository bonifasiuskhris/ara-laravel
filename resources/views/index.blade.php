@extends('layouts.app')

@section('content')

<div id="first" class="bg-full bg-first">
    <div class="sidebar-head"></div>
        <div class="container">
          <div class="row">
            <div class="col-md-4 sidebar">
              <div class="side-header-content text-white px-4">
                <img class="py-3" src="{{ asset('img/logo-icon.png') }}" alt="ARA" style="height:300px;">
                <h1>
                  <strong>ARA</strong> & ASSOCIATES</h1>
                <p>
                  <strong>We Protect Your Rights</strong>
                </p>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="second" class="bg-full bg-white">
    <div class="sidebar-back"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-4 sidebar">
                <div class="sidebar-content text-white px-4">
                    <h1><strong>PRACTICE</strong> AREAS</h1>
                </div>
            </div>
            <div class="col-md-4 pt-ara">
                <ul class="list text-ara-brown">
                <li class="my-4">
                    <span class="text-ara-red">Litigation and Dispute Settlement</span>
                </li>
                <li class="my-4">
                    <span class="text-ara-red">Civil and Criminal Law</span>
                </li>
                <li class="my-4">
                    <span class="text-ara-red">Corporate and Commercial Litigation</span>
                </li>
                <li class="my-4">
                    <span class="text-ara-red">Mining, Energy and Natural Resources</span>
                </li>
                <li class="my-4">
                    <span class="text-ara-red">Oil and Gas</span>
                </li>
                <li class="my-4">
                    <span class="text-ara-red">Employment and Industrial Relationship</span>
                </li>
                </ul>
            </div>
            <div class="col-md-4 pt-ara">
                <ul class="list text-ara-brown">
                <li class="my-4">
                    <span class="text-ara-red">Banking, Financial Service, and Insurance</span>
                </li>
                <li class="my-4">
                    <span class="text-ara-red">Intellectual Property Right</span>
                </li>
                <li class="my-4">
                    <span class="text-ara-red">Real Property and Land</span>
                </li>
                <li class="my-4">
                    <span class="text-ara-red">Divorce and Inheritance</span>
                </li>
                <li class="my-4">
                    <span class="text-ara-red">Research on Laws / Regulation</span>
                </li>
                <li class="my-4">
                    <a href="" class="btn btn-primary">DETAILS</a>
                </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="bg-full bg-ara-red">
    <div class="container">
        <div class="col-12 mx-auto text-center text-white pb-ara">
        <h1 class="py-ara">
            <strong>OUR</strong> VISION</h1>
        <p>ARA & Associates Law Offices (hereinafter shall be referred to as the “Firm”) was formed in early 2013, by its founding
            members Mr. Arif and Mr. Rama. Focused on litigation and corporate matters, the Firm has developed for serving best
            interest client, work to achieve client goal under prevailing Indonesia laws and regulation, and committed to providing
            professional legal service and timely and cost-sensitive representation to our clients. We are full of young, dynamic,
            and intelligence people, operating at a definite quality management system, rendering preeminent and the world-class
            legal. services under a specified quality code, core values.</p>
        <p>We are delighted to work exceptional service and provide a high-quality service also an ability to give outstanding
            local and international advice as well as an integrated full service. In seeking a legal solution, the Firm always
            adopts progressive and proactive in the practice of the law, based on relevant local knowledge. We seek to remain
            to be Firm of choice for those who desire highly competent and responsive lawyers to assist them in many activities
            in Indonesia. We take pride in being able to deliver top-notch specialized services as mentioned above. Indeed, we
            honor the trust of our clients in appointing us. Thus we always commit and strive to give the utmost level of expertise,
            attention, and supervision to the work which we accept.</p>
        <p>The Firmare the finest professional in their field. Experienced and capable of competing in what we started. The interest
            and enthusiasm for clearing and finishing cases are our main concern. </p>
        </div>
    </div>
</div>

<div class="bg-full bg-att">
</div>

<div class="bg-full bg-white">
    <div class="container pb-ara">
        <div class="row">
        <div class="col-md-12 text-center pt-ara pb-3">
            <h1 class="text-ara-red"><strong>OUR</strong> ATTORNEYS</h1>
        </div>
        <div class="col-md-4 offset-md-2 text-center">
            <img class="img-fluid" src="{{ asset('img/at-1.jpg') }}" alt="ARA" height="350px">
            <p class="sz-20">Mr. Arif Fadillah <br>Arifin, S.H., M.H. <br><span class="text-ara-brown">MANAGING PARTNER</span></p>
            <hr>
        </div>
        <div class="col-md-4 text-center">
            <img class="img-fluid" src="{{ asset('img/at-2.jpg') }}" alt="ARA" height="350px">
            <p class="sz-20">Mr. Rama Difa,<br> S.H., M.H. <br><span class="text-ara-brown">PARTNER</span></p>
            <hr>
        </div>
        <div class="col-md-12 text-center mt-4">
            <a href="" class="btn btn-primary">DETAILS</a>
        </div>
        </div>
    </div>
</div>

<div class="bg-full bg-ara-red">
    <div class="container">
        <div class="col-12 text-center text-white">
            <h1 class="pt-ara"><strong>GALLERY</strong></h1>
        </div>
    </div>
</div>

<div class="bg-full">
    <div class="container">
        <div class="col-12 text-center text-ara-red">
            <h1 class="py-ara"><strong>OUR</strong> CLIENTS</h1>
        </div>
        <div class="row text-center pb-ara">
            <div class="col-md-4">
                <img src="{{ asset('img/c-1.jpg') }}" alt="" class="img-fluid sepia my-2">
            </div>
            <div class="col-md-4">
                <img src="{{ asset('img/c-2.jpg') }}" alt="" class="img-fluid sepia my-2">
            </div>
            <div class="col-md-4">
                <img src="{{ asset('img/c-3.jpg') }}" alt="" class="img-fluid sepia my-2">
            </div>
            <div class="col-md-4 offset-md-2">
                <img src="{{ asset('img/c-4.jpg') }}" alt="" class="img-fluid sepia my-2">
            </div>
            <div class="col-md-4">
                <img src="{{ asset('img/c-5.jpg') }}" alt="" class="img-fluid sepia my-2">
            </div>
        </div>
    </div>
</div>

<div class="bg-full bg-contact">
    <div class="sidebar-back"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-4 sidebar text-white">
                <div class="sidebar-content text-white px-4">
                    <h1>CONTACT<br>ARA</h1>
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-7 text-white">
                <address class="pt-ara">
                    <h4><strong>OUR</strong> ADDRESS</h4>
                    <p>
                        <br>Gedung PKMI Pusat Lantai 1, Unit 2​​
                        <br>Jl.Kramat Sentiong, No. 49A, Senen, Jakarta Pusat
                        <br>Email: mail@ara-lawoffice.com​<br>
                    </p>
                        <a href=""><h5><strong>Click Here to Find Us</strong></h5></a>
                    <br>
                    <p>For any general inquiries, please fill in the following contact form:
                    </p>
                </address>
                <form class="pb-ara">
                    <div class="form-group">
                        <input type="text" class="form-control form-control-sm" id="contactName" placeholder="Name">
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control form-control-sm" id="contactEmail" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control form-control-sm" id="contactSubject" placeholder="Subject">
                    </div>
                    <div class="form-group">
                        <textarea class="form-control form-control-sm" id="contactMessage" rows="3" placeholder="Message"></textarea>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
