<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700|Raleway:400,700" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-51023171-9"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-51023171-9');
    </script>
</head>
<body>
    <div id="app">
        <nav class="navbar fixed-top navbar-expand-lg navbar-light bg-white border-bottom">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="{{ asset('img/logo-ara.png') }}" alt="{{ config('app.name', 'Laravel') }}" height="50px" />
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto text-uppercase">
                        {{--  <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest  --}}
                        <li class="nav-item px-2 py-2">
                            <a href="/" class="nav-link">home</a>
                        </li>
                        <li class="nav-item px-2 py-2">
                            <a href="/about" class="nav-link">about us</a>
                        </li>
                        <li class="nav-item px-2 py-2">
                            <a href="/practice" class="nav-link">practice areas</a>
                        </li>
                        <li class="nav-item px-2 py-2">
                            <a href="" class="nav-link">news</a>
                        </li>
                        <li class="nav-item px-2 py-2">
                            <a href="/contact" class="nav-link">contact</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <main>
            @yield('content')
        </main>

        <div class="container mt-3">
            <div class="row">
                <div class="col-md-12 text-center">
                    <img class="my-3 mx-1" src="{{ asset('img/logo-ara.png') }}" alt="ARA" height="65px">
                </div>
                <div class="col-md-12 text-center">
                    <img class="my-3 mx-1" src="{{ asset('img/logo-aai.jpeg') }}" alt="AAI" height="50px">
                    <img class="my-3 mx-1" src="{{ asset('img/logo-peradi.jpeg') }}" alt="PERADI" height="50px">
                </div>
                <div class="col-md-12 text-center">
                <p class="text-ara-red">© 2018 by ARA & ASSOCIATES</p>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
