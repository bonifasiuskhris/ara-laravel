@extends('layouts.app')

@section('content')

<div class="bg-brc">
    <div class="container">
        <div class="text-center text-white py-ara">
            <h1 class="text-head"><strong>PRACTICE</strong> AREAS</h1>
        </div>
    </div>
</div>

<div class="bg-full">
    <div class="container">
        <div class="row">
            <div class="col-12 text-justify my-ara">
                <h1 class="pt-ara pb-4">
                    <strong>ARA</strong> & ASSOCIATES
                </h1>
                <p>
                    <span class="text-ara-red"><strong>Litigation and Dispute Settlement</strong></span><br>
                    We have severally requested by our clients to represent and handle any disputes in various court jurisdiction, such as district court, religious court, state administrative court, labor court, arbitration, Islamic arbitration, appeal or cassation stage, including but not limited to enforce and/or suspend the court decision
                </p>
                <p>
                    <span class="text-ara-red"><strong>Civil and Criminal Law</strong></span><br>
                    Our Firm also have experiences in representing clients in various and at any level of courts in Indonesia, both civil and criminal, including commercial court for bankruptcy proceedings, intellectual property disputes, administrative court as well as in constitutional court for reviewing constitutionality of laws in Indonesia.
                </p>
                <p>
                    <span class="text-ara-red"><strong>Corporate and Commercial Litigation</strong></span><br>
                    Our legal service in the form of consultation, contracts drafting, legal due diligence, legal opinion, negotiating, in relation to establish a joint venture, merger, take-over, dissolution of a company, sale-purchase of company assets, corporate restructuring, public offering (go public), and others. We also provides legal services in establishing a limited liability company (PT), domestic capital investment and foreign capital investment (PT PMA), partnership, foundations, International Non Government Organization and cooperatives, and the last we provides a corporate secretarial services in arranging of all company document including deeds of incorporation, amendments to the company articles of association, and others.
                </p>
                <p>
                    <span class="text-ara-red"><strong>Mining, Energy and Natural Resources</strong></span><br>
                    We have supported and represented many clients in mining and energy sectors, either fossil or non-fossil energy, and mining services companies, especially related to the compliances with the prevailing regulations and obtaining any required licenses in relation tothe operation of their companies
                </p>
                <p>
                    <span class="text-ara-red"><strong>Oil and Gas</strong></span><br>
                    We have been deeply involved with numerous oil and gas exploration and exploitation transactions, including the negotiation of production sharing contracts and technical assistance contracts, as well as drilling rig contracts, services agreements, charter parties and other agreements customary in upstream activities. We are also extensively involved in downstream business activities, including LPG and LNG projects.
                </p>
                <p>
                    <span class="text-ara-red"><strong>Employment and Industrial Relationship</strong></span><br>
                    We provides legal service in handling manpower, which includes the drafting or reviewing work contract/agreement, company regulations or collective labor agreement, advising on termination of employment and other labor problems in general and arranging for work permit, visas, and other documents related to immigration affairs
                </p>
                <p>
                    <span class="text-ara-red"><strong>Banking, Financial Service, and Insurance</strong></span><br>
                    We have requested by several clients in banking / financial services / insurance companies to support their business through conducting legal due diligence, providing legal opinions on mandatory rules and compliance with prevailing regulations, and processing claim to their customers or debtors.
                </p>
                <p>
                    <span class="text-ara-red"><strong>Intellectual Property Right</strong></span><br>
                    We are expert and certified to handle and settle every dispute on intellectual property, including registering trademark, copyright, patent, industrial design and other rights of intellectual property
                </p>
                <p>
                    <span class="text-ara-red"><strong>Real Property and Land</strong></span><br>
                    We have provided the clients with legal opinions and support them to process the land acquisition either for individual, industry or real estate, especially on execution of security of seizure or assets that has been decided by courts, including to settle with the local community in order to completion of such land acquisition
                </p>
                <p>
                    <span class="text-ara-red"><strong>Divorce and Inheritance</strong></span><br>
                    We have severally requested by clients either foreigner or domestic individuals to handle and assist them in order to settle their divorce or inheritance in religious judicature or district court
                </p>
                <p>
                    <span class="text-ara-red"><strong>Research on Laws / Regulation</strong></span><br>
                    Our legal service in conducting legal research with respect to client’s inquiries based on the prevailing laws and regulations of Indonesia and will issue our legal advice/opinion in the form of Legal Memorandum / Opinion.
                </p>
            </div>
        </div>
    </div>
</div>

@endsection
